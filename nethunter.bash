# -----------------------------------------------------------------------------
# Copyright (C) 2020-2022 Aditya Shakya <adi1090x@gmail.com>
# Everyone is permitted to copy and distribute copies of this file under GNU-GPL3
#
# Bouquet color scheme
# -----------------------------------------------------------------------------

# Wallpaper
wallpaper='/usr/share/backgrounds/bouquet.jpg'

# Special
background='#000000'
foreground='#ffffff'
cursor='#ffffff'

# Colors
color0='#27292D'
color1='#d74b39'
color2='#aad94c'
color3='#fea129'
color4='#00e59b'
color5='#ba68c8'
color6='#89f4cd'
color7='#d8d8d8'
color8='#ffffff'
color9='#d74b39'
color10='#aad94c'
color11='#fea129'
color12='#00e59b'
color13='#ba68c8'
color14='#89f4cd'
color15='#fdf6e3'